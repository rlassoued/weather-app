package fr.test.weather.data.model

import com.google.gson.annotations.SerializedName

//
// Created by ramzi.lassoued  on 24/11/2020.
//
data class WeatherDto(
    @SerializedName("name") val name: String,
    @SerializedName("main") val main: Main,
    @SerializedName("dt") val date: Long,
    @SerializedName("weather") val weatherMain: List<WeatherMain>
)