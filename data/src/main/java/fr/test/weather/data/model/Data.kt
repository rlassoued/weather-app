package fr.test.weather.data.model

import com.google.gson.annotations.SerializedName

//
// Created by ramzi.lassoued  on 24/11/2020.
//
data class Data(
    @SerializedName("cod") val code: String,
    @SerializedName("message") val message: String,
    @SerializedName("list") val list: List<WeatherDto>?
)