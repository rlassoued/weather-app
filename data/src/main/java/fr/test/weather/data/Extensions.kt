package fr.test.weather.data

import fr.test.weather.data.model.WeatherDto
import fr.test.weather.domain.models.Weather
import kotlin.math.roundToInt

//
// Created by ramzi.lassoued  on 25/11/2020.
//
fun WeatherDto.toWeather() = Weather(
    date,
    main.temp.roundToInt(),
    main.feelsLike.roundToInt(),
    main.tempMin.roundToInt(),
    main.tempMax.roundToInt(),
    main.pressure,
    main.humidity,
    weatherMain.first().description,
    weatherMain.first().icon
)