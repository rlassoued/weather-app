package fr.test.weather.data.datasource.remote

import fr.test.weather.data.datasource.remote.api.WeatherService
import fr.test.weather.data.toWeather
import fr.test.weather.domain.usescases.WeatherResult

//
// Created by ramzi.lassoued  on 24/11/2020.
//
class RemoteDataStore(private val api: WeatherService) : DataSource {
    override suspend fun fetchWeatherDetails(query: String): WeatherResult {
        api.retrieveWeatherDetails(query).body()?.list?.first()?.let {
            return WeatherResult.Success(it.toWeather())
        }
        return WeatherResult.Error
    }
}