package fr.test.weather.data

import fr.test.weather.data.datasource.remote.DataSource
import fr.test.weather.domain.repository.WeatherRepository

//
// Created by ramzi.lassoued  on 24/11/2020.
//
class WeatherRepositoryImpl(private val remoteDataStore: DataSource) : WeatherRepository {
    override suspend fun getWeatherDetails(town: String) = remoteDataStore.fetchWeatherDetails(town)
}