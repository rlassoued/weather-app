package fr.test.weather.data.datasource.remote.api

import fr.test.weather.data.model.Data
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

//
// Created by ramzi.lassoued  on 24/11/2020.
//
interface WeatherService {
    @GET("/data/2.5/find")
    suspend fun retrieveWeatherDetails(
        @Query("q") city: String,
        @Query("appid") appid: String = "36b8f55ec813d30ccb6565893a6b7d28",
        @Query("units") units: String = "metric"
    ): Response<Data>

    companion object {
        fun api() =
            Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(
                    OkHttpClient
                        .Builder()
                        .build()
                ).build().create(WeatherService::class.java)

    }
}