package fr.test.weather.data.datasource.remote

import fr.test.weather.domain.usescases.WeatherResult

//
// Created by ramzi.lassoued  on 25/11/2020.
//
interface DataSource {
    suspend fun fetchWeatherDetails(query: String): WeatherResult
}