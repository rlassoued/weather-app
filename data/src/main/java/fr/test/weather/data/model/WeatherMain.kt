package fr.test.weather.data.model

import com.google.gson.annotations.SerializedName

//
// Created by ramzi.lassoued  on 24/11/2020.
//
data class WeatherMain(
    @SerializedName("description") val description: String,
    @SerializedName("icon") val icon: String
)