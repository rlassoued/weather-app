package fr.test.weather.data

import fr.test.weather.data.datasource.remote.DataSource
import fr.test.weather.domain.models.Weather
import fr.test.weather.domain.usescases.WeatherResult
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

//
// Created by ramzi.lassoued  on 25/11/2020.
//
@RunWith(MockitoJUnitRunner::class)
class WeatherRepositoryImplTest {

    private val remoteDataStore = mock(DataSource::class.java)
    private val weatherRepositoryImpl = WeatherRepositoryImpl(remoteDataStore)

    @Test
    fun `test get Weather Details from repository called`() {
        //given
        //when
        runBlocking {
            weatherRepositoryImpl.getWeatherDetails("")
        }
        //then
        runBlocking {
            Mockito.verify(remoteDataStore).fetchWeatherDetails("")
        }
    }

    @Test
    fun `test get Weather Details when repository return error`() {
        //given
        //when
        runBlocking {
            Mockito.`when`(remoteDataStore.fetchWeatherDetails(""))
                .thenReturn(WeatherResult.Error)
        }
        //then
        runBlocking {
            assertEquals(WeatherResult.Error, weatherRepositoryImpl.getWeatherDetails(""))
        }
    }

    @Test
    fun `test get Weather Details when repository return success`() {
        //given
        //when
        runBlocking {
            Mockito.`when`(remoteDataStore.fetchWeatherDetails(""))
                .thenReturn(WeatherResult.Success(Weather()))
        }
        //then
        runBlocking {
            assertEquals(
                WeatherResult.Success(Weather()),
                weatherRepositoryImpl.getWeatherDetails("")
            )
        }
    }
}