package fr.test.weather.data

import fr.test.weather.data.model.Main
import fr.test.weather.data.model.WeatherDto
import fr.test.weather.data.model.WeatherMain
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*

//
// Created by ramzi.lassoued  on 25/11/2020.
//
class ExtensionsTest {

    @Test
    fun `test convert weatherDto to Weather entity`() {
        //when
        val date = Date().time
        val weatherDto = WeatherDto(
            "city",
            Main(12.5, 9.2, 7.3, 14.2, 1000, 100),
            date,
            arrayListOf(WeatherMain("description", "icon"))
        )
        val weather = weatherDto.toWeather()
        //then
        assertEquals(date, weather.currentTime)
        assertEquals(9, weather.feelsLike)
        assertEquals(100, weather.humidity)
        assertEquals(1000, weather.pressure)
        assertEquals(13, weather.temp)
        assertEquals(14, weather.tempMax)
        assertEquals(7, weather.tempMin)
        assertEquals("description", weather.weatherDescription)
        assertEquals("icon", weather.weatherIcon)
    }
}