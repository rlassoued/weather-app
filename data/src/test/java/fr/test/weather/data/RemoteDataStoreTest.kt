package fr.test.weather.data

import fr.test.weather.data.datasource.remote.RemoteDataStore
import fr.test.weather.data.datasource.remote.api.WeatherService
import fr.test.weather.data.model.Data
import fr.test.weather.data.model.Main
import fr.test.weather.data.model.WeatherDto
import fr.test.weather.data.model.WeatherMain
import fr.test.weather.domain.models.Weather
import fr.test.weather.domain.usescases.WeatherResult
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock
import retrofit2.Response
import java.util.*

//
// Created by ramzi.lassoued  on 25/11/2020.
//
class RemoteDataStoreTest {

    private val mockApi = mock(WeatherService::class.java)
    private val remoteDataStore = RemoteDataStore(mockApi)

    @Test
    fun `test fetch weather details return error for empty query`() {
        //given
        //when
        runBlocking {
            Mockito.`when`(mockApi.retrieveWeatherDetails(""))
                .thenReturn(Response.success(Data("400", "error", null)))
        }
        //then
        runBlocking {
            assertEquals(WeatherResult.Error, remoteDataStore.fetchWeatherDetails(""))
        }
    }

    @Test
    fun `test fetch weather details return success for not empty query`() {
        //given
        val weatherDto = WeatherDto(
            "city",
            Main(12.5, 9.2, 7.3, 14.2, 1000, 100),
            Date().time,
            arrayListOf(WeatherMain("description", "icon"))
        )
        //when
        runBlocking {
            Mockito.`when`(mockApi.retrieveWeatherDetails("test"))
                .thenReturn(Response.success(Data("200", "test", arrayListOf(weatherDto))))
        }
        //then
        runBlocking {
            assertEquals(WeatherResult.Success(weatherDto.toWeather()), remoteDataStore.fetchWeatherDetails("test"))
        }
    }
}