package fr.test.weather.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.test.weather.domain.models.Weather
import fr.test.weather.domain.usescases.GetWeatherDetailsUseCase
import fr.test.weather.domain.usescases.WeatherResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class WeatherDetailsViewModel(private val weatherDetailsUseCase: GetWeatherDetailsUseCase) :
    ViewModel() {
    private val _viewAction = MutableLiveData<ViewAction>()
    val viewAction: LiveData<ViewAction> = _viewAction

    init {
        loadWeatherDetails()
    }

    private fun loadWeatherDetails() {
        _viewAction.value = ViewAction.ShowLoading
        viewModelScope.launch {
            val deferred = viewModelScope.async(Dispatchers.Default) {
                weatherDetailsUseCase.getWeatherDetails()
            }
            handleResult(deferred.await())
        }
    }

    private fun handleResult(result: WeatherResult) {
        _viewAction.value = ViewAction.HideLoading
        _viewAction.value = when (result) {
            is WeatherResult.Success -> {
                ViewAction.Content(result.weather)
            }
            else -> ViewAction.Error
        }
    }

    sealed class ViewAction {
        data class Content(val weather: Weather) : ViewAction()
        object Error : ViewAction()
        object ShowLoading : ViewAction()
        object HideLoading : ViewAction()
    }
}