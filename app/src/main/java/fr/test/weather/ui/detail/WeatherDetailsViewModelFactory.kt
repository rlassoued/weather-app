package fr.test.weather.ui.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import fr.test.weather.domain.usescases.GetWeatherDetailsUseCase

//
// Created by ramzi.lassoued  on 26/11/2020.
//
class WeatherDetailsViewModelFactory(private val weatherDetailsUseCase: GetWeatherDetailsUseCase) :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        WeatherDetailsViewModel(weatherDetailsUseCase) as T
}