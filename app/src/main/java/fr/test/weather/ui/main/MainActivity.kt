package fr.test.weather.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import fr.test.weather.R
import fr.test.weather.databinding.MainActivityBinding
import fr.test.weather.ui.detail.WeatherDetailsActivity
import fr.test.weather.ui.detail.WeatherDetailsActivity.Companion.KEY_TOWN

class MainActivity : AppCompatActivity() {

    private lateinit var binding: MainActivityBinding
    private val onItemClickListener = object : MainAdapter.OnItemClickListener {
        override fun onItemClick(position: Int) {
            val intent = Intent(this@MainActivity, WeatherDetailsActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            intent.putExtra(KEY_TOWN, mainAdapter.getItem(position))
            startActivity(intent)
        }
    }
    private val mainAdapter: MainAdapter by lazy { MainAdapter(onItemClickListener) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.townRecyclerView.adapter = mainAdapter
        setupViews()
    }

    private fun setupViews() {
        refreshVisibilityViews()
        binding.addTown.setOnClickListener {
            //Inflate the dialog with custom view
            val editText = EditText(this)
            //AlertDialogBuilder
            val alertDialog = AlertDialog.Builder(this)
                .setView(editText)
                .setTitle(getString(R.string.new_town))
                .setCancelable(false)
                .setPositiveButton(
                    R.string.ok
                ) { _, _ ->
                    if (editText.text.toString().isEmpty()) {
                        Snackbar.make(it, R.string.error_add_empty_town, Snackbar.LENGTH_LONG)
                            .show()
                    } else {
                        if (mainAdapter.isAlreadyExist(editText.text.toString())) {
                            Snackbar.make(
                                it,
                                R.string.error_town_already_exist,
                                Snackbar.LENGTH_LONG
                            )
                                .show()
                        } else {
                            mainAdapter.addNewTown(editText.text.toString())
                            refreshVisibilityViews()
                        }
                    }
                }
                .setNegativeButton(
                    R.string.cancel
                ) { _, _ -> }
                .create()
            //show dialog
            alertDialog.show()
        }
    }

    private fun refreshVisibilityViews() {
        if (mainAdapter.itemCount == 0) {
            binding.townRecyclerView.visibility = GONE
            binding.header.visibility = GONE
        } else {
            binding.emptyList.visibility = GONE
            binding.townRecyclerView.visibility = VISIBLE
            binding.header.visibility = VISIBLE
        }
    }
}