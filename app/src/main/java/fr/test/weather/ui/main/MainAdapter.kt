package fr.test.weather.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import fr.test.weather.databinding.TownItemBinding


//
// Created by ramzi.lassoued  on 25/11/2020.
//
class MainAdapter(
    private val onItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var townList = arrayListOf<String>()
    private lateinit var townItemBinding: TownItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        townItemBinding =
            TownItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TownViewHolder(townItemBinding)
    }

    override fun getItemCount(): Int = townList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        townItemBinding.townName.text = townList[position]
        townItemBinding.root.setOnClickListener {
            onItemClickListener.onItemClick(position)
        }
    }

    fun getItem(position: Int) = townList[position]

    fun addNewTown(town: String) {
        townList.add(town)
        notifyDataSetChanged()
    }

    fun isAlreadyExist(town: String) = townList.contains(town)

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    inner class TownViewHolder(binding: TownItemBinding) : RecyclerView.ViewHolder(binding.root)
}
