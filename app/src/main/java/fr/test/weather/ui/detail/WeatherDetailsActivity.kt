package fr.test.weather.ui.detail

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import fr.test.weather.R
import fr.test.weather.data.WeatherRepositoryImpl
import fr.test.weather.data.datasource.remote.RemoteDataStore
import fr.test.weather.data.datasource.remote.api.WeatherService
import fr.test.weather.databinding.WeatherDetailsActivityBinding
import fr.test.weather.domain.models.Weather
import fr.test.weather.domain.usescases.GetWeatherDetailsUseCase
import kotlinx.android.synthetic.main.weather_details_activity.*
import java.text.SimpleDateFormat
import java.util.*

class WeatherDetailsActivity : AppCompatActivity() {

    private lateinit var binding: WeatherDetailsActivityBinding
    private lateinit var weatherDetailsUseCase: GetWeatherDetailsUseCase
    private val weatherDetailsViewModel: WeatherDetailsViewModel by viewModels {
        WeatherDetailsViewModelFactory(
            weatherDetailsUseCase
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = WeatherDetailsActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val town = intent.getStringExtra(KEY_TOWN)
        weatherDetailsUseCase = GetWeatherDetailsUseCase(
            WeatherRepositoryImpl(
                RemoteDataStore(
                    WeatherService.api()
                )
            ), town!!
        )
        weatherDetailsViewModel.viewAction.observe(this, Observer {
            handleViewAction(it)
        })
    }

    private fun handleViewAction(viewAction: WeatherDetailsViewModel.ViewAction?) {
        when (viewAction) {
            is WeatherDetailsViewModel.ViewAction.Content -> {
                refreshViews(viewAction.weather)
            }
            is WeatherDetailsViewModel.ViewAction.Error -> refreshViews(null)
            is WeatherDetailsViewModel.ViewAction.ShowLoading -> loading.visibility = VISIBLE
            is WeatherDetailsViewModel.ViewAction.HideLoading -> loading.visibility = GONE
        }
    }

    private fun refreshViews(weather: Weather?) {
        binding.error.visibility = if (weather == null) VISIBLE else GONE
        binding.date.apply {
            visibility = if (weather != null) VISIBLE else GONE
            weather?.let {
                text =
                    SimpleDateFormat(
                        "EEE, d MMM yyyy",
                        Locale.getDefault()
                    ).format(Date(weather.currentTime!! * 1000)).toString()
            }
        }
        binding.weatherIcon.apply {
            visibility = if (weather != null) VISIBLE else GONE
            weather?.let {
                Glide.with(this@WeatherDetailsActivity)
                    .load("$URL_WEATHER_ICON${it.weatherIcon}@4x.png").into(this)
            }
        }
        binding.weatherDescription.apply {
            visibility = if (weather != null) VISIBLE else GONE
            weather?.let { text = weather.weatherDescription }
        }
        binding.temperature.apply {
            visibility = if (weather != null) VISIBLE else GONE
            weather?.let { text = getString(R.string.temp).format(weather.temp) }
        }
        binding.temperatureFeelLike.apply {
            visibility = if (weather != null) VISIBLE else GONE
            weather?.let {
                text = getString(R.string.temp_feels_like).format(weather.feelsLike)
            }
        }
        binding.temperatureMin.apply {
            visibility = if (weather != null) VISIBLE else GONE
            weather?.let {
                text = getString(R.string.temp_min).format(weather.tempMin)
            }
        }
        binding.temperatureMax.apply {
            visibility = if (weather != null) VISIBLE else GONE
            weather?.let {
                text = getString(R.string.temp_max).format(weather.tempMax)
            }
        }
        binding.pressure.apply {
            visibility = if (weather != null) VISIBLE else GONE
            weather?.let {
                text = getString(R.string.pressure).format(weather.pressure)
            }
        }
        binding.humidity.apply {
            visibility = if (weather != null) VISIBLE else GONE
            weather?.let {
                text = getString(R.string.humidity).format(weather.humidity)
            }
        }
    }

    companion object {
        const val KEY_TOWN = "town"
        const val URL_WEATHER_ICON = "http://openweathermap.org/img/wn/"
    }
}