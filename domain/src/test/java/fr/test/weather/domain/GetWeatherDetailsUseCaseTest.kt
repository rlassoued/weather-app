package fr.test.weather.domain

import fr.test.weather.domain.models.Weather
import fr.test.weather.domain.repository.WeatherRepository
import fr.test.weather.domain.usescases.GetWeatherDetailsUseCase
import fr.test.weather.domain.usescases.WeatherResult
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

//
// Created by ramzi.lassoued  on 24/11/2020.
class GetWeatherDetailsUseCaseTest {

    private val TOWN = "paris"
    private val weatherRepository = mock(WeatherRepository::class.java)
    private val getWeatherDetailsUseCase = GetWeatherDetailsUseCase(weatherRepository, TOWN)

    @Test
    fun `test get Weather Details from repository called`() {
        //given
        //when
        runBlocking {
            getWeatherDetailsUseCase.getWeatherDetails()
        }
        //then
        runBlocking {
            Mockito.verify(weatherRepository).getWeatherDetails(TOWN)
        }
    }

    @Test
    fun `test get Weather Details when repository return error`() {
        //given
        //when
        runBlocking {
            Mockito.`when`(weatherRepository.getWeatherDetails(TOWN))
                .thenReturn(WeatherResult.Error)
        }
        //then
        runBlocking {
            assertEquals(WeatherResult.Error, getWeatherDetailsUseCase.getWeatherDetails())
        }
    }

    @Test
    fun `test get Weather Details when repository return success`() {
        //given
        //when
        runBlocking {
            Mockito.`when`(weatherRepository.getWeatherDetails(TOWN))
                .thenReturn(WeatherResult.Success(Weather()))
        }
        //then
        runBlocking {
            assertEquals(
                WeatherResult.Success(Weather()),
                getWeatherDetailsUseCase.getWeatherDetails()
            )
        }
    }

}