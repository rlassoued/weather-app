package fr.test.weather.domain.usescases

import fr.test.weather.domain.models.Weather

//
// Created by ramzi.lassoued  on 24/11/2020.
//

sealed class WeatherResult {
    data class Success(val weather: Weather) : WeatherResult()
    object Error : WeatherResult()
}
