package fr.test.weather.domain.repository

import fr.test.weather.domain.usescases.WeatherResult

//
// Created by ramzi.lassoued  on 23/11/2020.
//
interface WeatherRepository {
    /**
     * fetch weather details from repository
     * @return WeatherResult
     */
    suspend fun getWeatherDetails(town: String): WeatherResult
}