package fr.test.weather.domain.models

//
// Created by ramzi.lassoued  on 23/11/2020.
//
data class Weather(
    var currentTime: Long? = null,
    var temp: Int? = null,
    var feelsLike: Int? = null,
    var tempMin: Int? = null,
    var tempMax: Int? = null,
    var pressure: Int? = null,
    var humidity: Int? = null,
    var weatherDescription: String? = null,
    var weatherIcon: String? = null
)