package fr.test.weather.domain.usescases

import fr.test.weather.domain.repository.WeatherRepository

//
// Created by ramzi.lassoued  on 24/11/2020.
//
class GetWeatherDetailsUseCase(
    private val weatherRepository: WeatherRepository,
    private val town: String
) {
    suspend fun getWeatherDetails() = weatherRepository.getWeatherDetails(town)
}